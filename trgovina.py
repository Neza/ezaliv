# -*- coding: utf-8 -*-
from bottle import route, run, template, error, post, request, redirect, response

def preberi_kategorije():
    with open('./data/kategorije.txt', encoding='utf-8') as f:
        return {k: v for k, v in enumerate(x.strip() for x in f.readlines())}

def preberi_izdelke(kat=None):  # kat pove izdelke iz katere kategorije bi radi prebrali
    ret = {}
    id_ = 0
    with open('./data/izdelki.txt', encoding='utf-8') as f:
        while True:
            ime = f.readline().strip()
            if ime == '':
                break
            kategorija = int(f.readline())
            cena = float(f.readline())
            zaloga = int(f.readline())
            opis = f.readline().strip()
            if (kat is None) or (kategorija == kat):
                ret[id_] = {'ime': ime, 'kategorija': kategorija, 'cena': cena, 'zaloga': zaloga, 'opis': opis}
            id_ += 1
    return ret

@route('/kategorija/<kat:int>')  # dekoratorcek; naslov dopolnimo s kategorija/celo stevilo, potem odpre nekaj ...
def prikaz_izdelkov(kat):
    return template('kategorija', ime=preberi_kategorije()[kat], izdelki=preberi_izdelke(kat))

def veljaven_racun(uporabnik, geslo):
    with open('.data/administratorji.txt', encoding='utf-8'):
        admins = dict([line.split(',')for line in f.readlines])
    return (uporabnik in admins) and (admins[uporabnik] == geslo)

@route('/admin')  # kadar ima nas naslov 7admin na koncu, takrat pridem na administratorsko podrocje
def admin_podrocje():
    uporabnik = request.get_cookie('racun', secret='sirov.burek')  # geslo sirov.burek vemo le mi, uporabnik ne, zato ne morejo ponaredit piskotkov kar tako
    if uporabnik:
        return 'Uspelo!'  # testiramo, ce deluje
    else:
        return redirect('/admin/login')

@route('/admin/login')  # ko se ne posljemo podatkov, ko samo pridemo na to stran, obmo le prikazali obrazec
def admin_login():
    return template('login')

@post('/admin/login')
def do_login():
    uporabnik = request.forms.get('uporabnik')  # v oklepaju mora biti isti name kot v login.tpl
    geslo = request.forms.get('geslo')
    if veljaven_racun(uporabnik, geslo):
        response.set_cookie('racun', uporabnik, secret='sirov.burek')
        return redirect('/admin')
    else:
        return redirect('/')

@route('/')
def prikaz_kategorij():
    return template('home', kategorije=preberi_kategorije())

"""
@error(404)
def error_404(err):
    return 'Napaka 404!'
"""
# nastavim sam svoje privzeto opozorilo, kar torej hocem, da se izpise
@error(404)
def error_404(err):
    return '<html><head></head><body>Napaka 404!</body></html>'

run(host='localhost', port=8080, debug=True, reloader=True)
# pozene ta servercek, debug nam izpisuje sporocila, kaj se trenutno dogaja z nasim streznikom
